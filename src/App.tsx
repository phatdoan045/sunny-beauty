import React, { Component } from 'react';
import  Skeleton  from './components/Skeleton/Skeleton';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import './App.css'
import Loading from './components/Loading/Loading';
class App extends Component {
  render() {
    return (
      <div className="Layout__App">
            <Skeleton/>
            <Loading/>
      </div>
    );
  }
}
export default App;