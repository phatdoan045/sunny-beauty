import React, { Component } from "react";
import loading_icon from "../../assets/images/icons/loading.svg";

// import redux
import { connect } from "react-redux";

// import scss
import "./Loading.scss";
interface IProps {
  loadingReducers?: boolean;
}

class Loading extends Component<IProps> {
  render() {
    let { loadingReducers } = this.props;
    return (
      <>
        {loadingReducers ? (
          <div className="layout_loading">
            <img
              src={loading_icon}
              alt="loading"
              className="layout_loading_detail"
            />
          </div>
        ) : null}
      </>
    );
  }
}

export default connect(
  (state: any) => ({
    loadingReducers: state.loadingReducers,
  }),
  null
)(Loading);
