import React, { Component } from "react";
import { urlServerImageProduct } from "../../../configAPI/local";

import DeleteOutlineOutlinedIcon from "@material-ui/icons/DeleteOutlineOutlined";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";

import { Switch } from "antd";

import "./ItemProduct.scss";

interface IProps {
  product: any;
  keyProduct: number;
  page: number;
  limit: number;
}
class ItemProduct extends Component<IProps> {
  render() {
    let { product, keyProduct, page, limit } = this.props;
    return (
      <>
        <tr className="layout__Itemproduct" key={keyProduct.toString()}>
          <td className="layout__Itemproduct-td">
            {keyProduct + 1 + limit * page - limit}
          </td>
          <td className="layout__Itemproduct-td layout__Itemproduct-name">
            {product.name}
          </td>
          <td className="layout__Itemproduct-td layout__Itemproduct-image">
            {product.productImage[0] ? (
              <img
                src={`${urlServerImageProduct}${product.productImage[0]}`}
                alt="imageProduct"
              />
            ) : (
              <p>No Image</p>
            )}
          </td>
          <td className="layout__Itemproduct-td">{4}</td>
          <td className="layout__Itemproduct-td">{product.barcode}</td>
          <td className="layout__Itemproduct-td">
            <Switch
              size="small"
              style={{ background: "#ef99ac" }}
              checked={product.status === 1 ? true : false}
            />
          </td>
          <td className="layout__Itemproduct-td">
            <EditOutlinedIcon />
            <DeleteOutlineOutlinedIcon />
          </td>
        </tr>
      </>
    );
  }
}

export default ItemProduct;
