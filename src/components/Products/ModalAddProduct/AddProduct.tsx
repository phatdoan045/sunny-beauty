import React, { Component } from "react";

// import ant design
import { Modal, Row, Col, Select, Input } from "antd";
// import Redux
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";
import { brandsActions } from "../../../stores/actions/Brands";
import { categoriesActions } from "../../../stores/actions/Categories";

// Import Untilities Function and Commponent
import ShowImageClient from "../../../componentsUtilities/ShowImageClient/ShowImageClient";
import {
  deleteItemByIndex,
  // formatNumberPrice,
} from "../../../functionsUtilities/index";
import InputUtil from "../../../componentsUtilities/InputUtil";

import CloudUploadOutlinedIcon from "@material-ui/icons/CloudUploadOutlined";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";

// Import scss
import "./AddProduct.scss";
import ShowColor from "./ShowColor/ShowColor";

const { Option } = Select;
const selectUnit = (
  <Select defaultValue="kg" className="select-after">
    <Option value="0">kg</Option>
    <Option value="1">ml</Option>
  </Select>
);

interface product {
  name: string;
  englishname: string;
  categoryId: string;
  description: string;
  origin?: string;
  brandId: string;
  inStock: number;
  capacity?: string;
  color: any;
  importPriceUSD: number;
  importPriceVND: number;
  importFee: number;
  retailPrice: number;
  wholeSalePrice: number;
  barcode: number;
  status: number;
  productImage?: [];
}
interface brandsAllReducers {
  brands: [];
}
interface brands {
  description?: string;
  brandImage?: string;
  _id: string;
  name: string;
}
interface category {
  description?: string;
  parentId?: string;
  _id: string;
  name: string;
}

interface categoriesAllReducers {
  categories: [];
}

interface IProps {
  visible: boolean;
  categoriesAllReducers: categoriesAllReducers;
  brandsAllReducers: brandsAllReducers;
  categoriesChildrenReducers: categoriesAllReducers;
  actionsCategories: any;
  actionsBrands: any;
  handleAddProduct: () => void;
}
interface IState {
  imageShowClientState: any;
  name: string;
  englishname: string;
  categoryId: string;
  description: string;
  origin?: string;
  brandId: string;
  inStock: string;
  capacity?: string;
  color: any;
  importPriceUSD: string;
  importPriceVND: string;
  importFee: string;
  retailPrice: string;
  wholeSalePrice: string;
  barcode: string;
  status: string;
  productImage?: [];
  cost: string;
  valueColor: string;
  files: any;
  categoryIdChilren: string;
  categoryIdParent: string;
}

class AddProduct extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      imageShowClientState: [],
      name: "",
      englishname: "",
      categoryId: "",
      description: "",
      origin: "",
      brandId: "",
      inStock: "",
      capacity: "",
      cost: "",
      color: [],
      importPriceUSD: "",
      importPriceVND: "",
      importFee: "",
      retailPrice: "",
      wholeSalePrice: "",
      barcode: "",
      status: "",
      valueColor: "",
      productImage: [],
      files: [],
      categoryIdChilren: "",
      categoryIdParent: "",
    };
  }
  componentDidMount() {
    this.props.actionsBrands.getListBrandAllAPI();
    this.props.actionsCategories.getListCategoriesAllAPI();
  }
  handleCloseModal = () => {
    this.props.handleAddProduct();
  };
  _handleUploadImageProduct = (e: any) => {
    if (e.target.files[0] && e.target.files && e.target.files.length <= 10) {
      let { files } = e.target;
      let { imageShowClientState } = this.state;
      var imageShowClient = imageShowClientState;
      for (var i = 0; i < files.length; i++) {
        let reader = new FileReader();
        reader.readAsDataURL(files[i]);
        reader.onload = (event: any) => {
          if (event.target) {
            imageShowClient.push(event.target.result);
          }
          this.setState({
            imageShowClientState: imageShowClient,
            files: [],
          });
        };
      }
    }
  };
  handleDeleteImage = (keyImage: number) => {
    let { imageShowClientState } = this.state;
    this.setState((prevState) => ({
      ...prevState,
      imageShowClientState: deleteItemByIndex(imageShowClientState, keyImage),
    }));
  };

  handleChangeInput = (event: any) => {
    event.persist();
    this.setState((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  handleChangeBrands = (value: string) => {
    this.setState((prevState) => ({
      ...prevState,
      brandId: value,
    }));
  };

  handleChangeCategory = (value: string) => {
    this.props.actionsCategories.getCategoriesByIdChildren(value);
    if (!this.props.categoriesChildrenReducers.categories) {
      this.setState((prevState) => ({
        ...prevState,
        categoryIdParent: value,
      }));
    }
  };

  handleChangeCategoryChildren = (value: string) => {
    this.setState((prevState) => ({
      ...prevState,
      categoryIdChilren: value,
    }));
  };
  handleChageColor = (event: any) => {
    this.setState({
      valueColor: event.target.value,
    });
  };
  handleClickAddColor = (event: any) => {
    let { valueColor, color } = this.state;
    if (valueColor && valueColor !== "") {
      let arrayColor = color;
      arrayColor.push(valueColor);
      this.setState({
        color: arrayColor,
        valueColor: "",
      });
    }
  };

  handleDeleteColor = (keyIndexColor: number) => {
    let { color } = this.state;
    this.setState((prevState) => ({
      ...prevState,
      color: deleteItemByIndex(color, keyIndexColor),
    }));
  };
  render() {
    let { visible } = this.props;
    let {
      imageShowClientState,
      name,
      englishname,
      categoryId,
      description,
      origin,
      brandId,
      inStock,
      capacity,
      color,
      importPriceUSD,
      importPriceVND,
      importFee,
      retailPrice,
      wholeSalePrice,
      barcode,
      // status,
      valueColor,
      cost,
      files,
    } = this.state;
    let {
      brandsAllReducers,
      categoriesAllReducers,
      categoriesChildrenReducers,
    } = this.props;
    return (
      <>
        <Modal
          destroyOnClose={true}
          visible={visible}
          cancelButtonProps={{ style: { display: "none" } }}
          okButtonProps={{ style: { display: "none" } }}
          footer={null}
          maskClosable={false}
          onCancel={this.handleCloseModal}
          width={"70%"}
          style={{ marginTop: "0px" }}
        >
          <Row className="layout__Modal-title">
            <Col span={24} className="layout__Modal-title--box">
              <span>Thêm sản phẩm</span>
            </Col>
          </Row>
          <Row className="layout__Modal-addProduct">
            <Col span={12} className="layout__Modal-addProduct_left">
              <InputUtil
                classnameRow="layout__Modal-addProduct_left-input"
                numberSpanColTitle={8}
                classNameColTitle="layout__Modal-addProduct_left-input--lable"
                nameTitle="Tên sản phẩm"
                numberSpanColContent={16}
                classNameColContent="layout__Modal-addProduct_left-input--detail"
                typeInput=""
                name="name"
                value={name}
                handleChangeInput={this.handleChangeInput}
              />

              <InputUtil
                classnameRow="layout__Modal-addProduct_left-input"
                numberSpanColTitle={8}
                classNameColTitle="layout__Modal-addProduct_left-input--lable"
                nameTitle="Tên sản phẩm (EN)"
                numberSpanColContent={16}
                classNameColContent="layout__Modal-addProduct_left-input--detail"
                typeInput=""
                name="englishname"
                value={englishname}
                handleChangeInput={this.handleChangeInput}
              />

              <InputUtil
                classnameRow="layout__Modal-addProduct_left-input"
                numberSpanColTitle={8}
                classNameColTitle="layout__Modal-addProduct_left-input--lable"
                nameTitle="Nguồn gốc"
                numberSpanColContent={16}
                classNameColContent="layout__Modal-addProduct_left-input--detail"
                name="origin"
                value={origin}
                handleChangeInput={this.handleChangeInput}
              />

              <InputUtil
                classnameRow="layout__Modal-addProduct_left-input"
                numberSpanColTitle={8}
                classNameColTitle="layout__Modal-addProduct_left-input--lable"
                nameTitle="Giá thành"
                numberSpanColContent={16}
                classNameColContent="layout__Modal-addProduct_left-input--detail"
                clasnameInputContent="input-typeNumber"
                name="cost"
                value={cost}
                typeInput="number"
                handleChangeInput={this.handleChangeInput}
              />

              <InputUtil
                classnameRow="layout__Modal-addProduct_left-input"
                numberSpanColTitle={8}
                classNameColTitle="layout__Modal-addProduct_left-input--lable"
                nameTitle="Giá nhập VND"
                numberSpanColContent={16}
                classNameColContent="layout__Modal-addProduct_left-input--detail"
                clasnameInputContent="input-typeNumber"
                name="importPriceVND"
                value={importPriceVND}
                handleChangeInput={this.handleChangeInput}
                typeInput="number"
              />

              <InputUtil
                classnameRow="layout__Modal-addProduct_left-input"
                numberSpanColTitle={8}
                classNameColTitle="layout__Modal-addProduct_left-input--lable"
                nameTitle="Giá nhập USD"
                numberSpanColContent={16}
                classNameColContent="layout__Modal-addProduct_left-input--detail"
                clasnameInputContent="input-typeNumber"
                name="importPriceUSD"
                typeInput="number"
                value={importPriceUSD}
                handleChangeInput={this.handleChangeInput}
              />

              <InputUtil
                classnameRow="layout__Modal-addProduct_left-input"
                numberSpanColTitle={8}
                classNameColTitle="layout__Modal-addProduct_left-input--lable"
                nameTitle="Phí vận chuyển"
                numberSpanColContent={16}
                classNameColContent="layout__Modal-addProduct_left-input--detail"
                typeInput="number"
                clasnameInputContent="input-typeNumber"
                name="importFee"
                value={importFee}
                handleChangeInput={this.handleChangeInput}
              />

              <InputUtil
                classnameRow="layout__Modal-addProduct_left-input"
                numberSpanColTitle={8}
                classNameColTitle="layout__Modal-addProduct_left-input--lable"
                nameTitle="Giá bán lẻ"
                numberSpanColContent={16}
                classNameColContent="layout__Modal-addProduct_left-input--detail"
                typeInput="number"
                clasnameInputContent="input-typeNumber"
                name="retailPrice"
                value={retailPrice}
                handleChangeInput={this.handleChangeInput}
              />

              <InputUtil
                classnameRow="layout__Modal-addProduct_left-input"
                numberSpanColTitle={8}
                classNameColTitle="layout__Modal-addProduct_left-input--lable"
                nameTitle=" Giá bán sĩ"
                numberSpanColContent={16}
                classNameColContent="layout__Modal-addProduct_left-input--detail"
                typeInput="number"
                clasnameInputContent="input-typeNumber"
                name="wholeSalePrice"
                value={wholeSalePrice}
                handleChangeInput={this.handleChangeInput}
              />

              <Row className="layout__Modal-addProduct_right-input">
                <Col
                  span={8}
                  className="layout__Modal-addProduct_right-input--lable"
                >
                  Danh mục
                  <span>(*)</span>
                </Col>
                <Col
                  span={16}
                  className="layout__Modal-addProduct_right-input--detail"
                >
                  <Select
                    showSearch
                    style={{ width: "90%" }}
                    placeholder="Danh mục"
                    optionFilterProp="children"
                    onChange={this.handleChangeCategory}
                    value={categoryId ? categoryId : undefined}
                  >
                    {categoriesAllReducers
                      ? categoriesAllReducers.categories.map(
                          (item: category, index: number) =>
                            !item.parentId ? (
                              <Option key={index} value={item._id}>
                                {item.name}
                              </Option>
                            ) : null
                        )
                      : null}
                  </Select>
                </Col>
              </Row>
              {categoriesChildrenReducers.categories.length >= 1 ? (
                <Row className="layout__Modal-addProduct_left-input">
                  <Col
                    span={8}
                    className="layout__Modal-addProduct_left-input--lable"
                  >
                    Danh mục con
                    <span>(*)</span>
                  </Col>
                  <Col
                    span={16}
                    className="layout__Modal-addProduct_left-input--detail"
                  >
                    <Select
                      showSearch
                      style={{ width: "90%" }}
                      placeholder="Danh mục"
                      optionFilterProp="children"
                      onChange={this.handleChangeCategoryChildren}
                      value={categoryId ? categoryId : undefined}
                    >
                      {categoriesChildrenReducers.categories.map(
                        (item: category, index: number) =>
                          item.parentId ? (
                            <Option key={index} value={item._id}>
                              {item.name}
                            </Option>
                          ) : null
                      )}
                    </Select>
                  </Col>
                </Row>
              ) : null}
            </Col>

            <Col span={12} className="layout__Modal-addProduct_right">
              <Row className="layout__Modal-addProduct_right-input">
                <Col
                  span={8}
                  className="layout__Modal-addProduct_right-input--lable"
                >
                  Thương hiệu
                  <span>(*)</span>
                </Col>
                <Col
                  span={16}
                  className="layout__Modal-addProduct_right-input--detail"
                >
                  <Select
                    showSearch
                    style={{ width: "100%" }}
                    placeholder="Thương hiệu"
                    optionFilterProp="children"
                    onChange={this.handleChangeBrands}
                    value={brandId ? brandId : undefined}
                  >
                    {brandsAllReducers
                      ? brandsAllReducers.brands.map(
                          (item: brands, index: number) => (
                            <Option key={index} value={item._id}>
                              {item.name}
                            </Option>
                          )
                        )
                      : null}
                  </Select>
                </Col>
              </Row>

              <Row className="layout__Modal-addProduct_right-input">
                <Col
                  span={8}
                  className="layout__Modal-addProduct_right-input--lable layout__Modal-addProduct_right-input-lable-long"
                >
                  Nhập khối lượng / Dung tích
                  <span>(*)</span>
                </Col>
                <Col
                  span={16}
                  className="layout__Modal-addProduct_right-input--detail "
                >
                  <Input
                    addonAfter={selectUnit}
                    type="number"
                    placeholder="Nhập khối lượng / Dung tích"
                    value={capacity}
                    onChange={this.handleChangeInput}
                    name="capacity"
                  />
                </Col>
              </Row>

              <InputUtil
                classnameRow="layout__Modal-addProduct_left-input"
                numberSpanColTitle={8}
                classNameColTitle="layout__Modal-addProduct_left-input--lable"
                nameTitle="Tồn kho"
                numberSpanColContent={16}
                classNameColContent="layout__Modal-addProduct_left-input--detail"
                typeInput="number"
                clasnameInputContent="input-typeNumber"
                name="inStock"
                value={inStock}
                handleChangeInput={this.handleChangeInput}
              />

              <InputUtil
                classnameRow="layout__Modal-addProduct_left-input"
                numberSpanColTitle={8}
                classNameColTitle="layout__Modal-addProduct_left-input--lable"
                nameTitle="Mã Vạch"
                numberSpanColContent={16}
                classNameColContent="layout__Modal-addProduct_left-input--detail"
                typeInput="number"
                clasnameInputContent="input-typeNumber"
                name="barcode"
                value={barcode}
                handleChangeInput={this.handleChangeInput}
              />

              <Row className="layout__Modal-addProduct_right-input">
                <Col
                  span={8}
                  className="layout__Modal-addProduct_right-input--lable layout__Modal-addProduct_right-input-lable-long"
                >
                  Mô tả
                  <span>(*)</span>
                </Col>
                <Col
                  span={16}
                  className="layout__Modal-addProduct_right-input--detail "
                >
                  <textarea
                    onChange={this.handleChangeInput}
                    placeholder="Mô tả"
                    name="description"
                    value={description}
                  ></textarea>
                </Col>
              </Row>

              <Row className="layout__Modal-addProduct_right-input">
                <Col
                  span={8}
                  className="layout__Modal-addProduct_right-input--lable layout__Modal-addProduct_right-input-lable-long"
                >
                  Màu sắc
                </Col>
                <Col
                  span={10}
                  className="layout__Modal-addProduct_right-input--detail layout__Modal-addProduct_right-input-codeColor"
                >
                  <input
                    placeholder="Mã màu"
                    onChange={this.handleChageColor}
                    value={valueColor}
                    name="valueColor"
                  ></input>
                </Col>
                <Col
                  span={6}
                  className="layout__Modal-addProduct_right-input-codeColor-box"
                >
                  <button
                    className="layout__Modal-addProduct_right-input-codeColor-box-bnt"
                    onClick={this.handleClickAddColor}
                  >
                    <AddOutlinedIcon className="layout__Modal-addProduct_right-input-codeColor-box-bnt-icon" />
                    <span>Thêm</span>
                  </button>
                </Col>
                <Col span={8}></Col>
                <Col
                  span={16}
                  className="layout__Modal-addProduct_right-input--detail "
                >
                  <Row>
                    {color ? (
                      color.map((item: any, index: number) => (
                        <ShowColor
                          item={item}
                          keyCodeColor={index}
                          handleDeleteColor={this.handleDeleteColor}
                        />
                      ))
                    ) : (
                      <div></div>
                    )}
                  </Row>
                </Col>
              </Row>

              <Row className="layout__Modal-addProduct_right-input">
                <Col
                  span={8}
                  className="layout__Modal-addProduct_right-input--lable layout__Modal-addProduct_right-input-lable-long"
                >
                  Mô tả (EN)
                  <span>(*)</span>
                </Col>
                <Col
                  span={16}
                  className="layout__Modal-addProduct_right-input--detail "
                >
                  <textarea placeholder="Mô tả"></textarea>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="layout__Modal-addProduct_Upload">
            <Col span={24}>
              <Row className="layout__Modal-addProduct_Upload-row">
                <Col
                  span={4}
                  className="layout__Modal-addProduct_Upload-row--label"
                >
                  Hình ảnh
                  <span>(*)</span>
                </Col>
                <Col
                  span={20}
                  className="layout__Modal-addProduct_Upload-row--Upload"
                >
                  <label
                    htmlFor="ProductPicture"
                    className="layout__Modal-addProduct_lableUpload"
                  >
                    <CloudUploadOutlinedIcon className="layout__Modal-addProduct_lableUpload--icon" />{" "}
                    <span>Chọn tệp</span>
                  </label>
                  <input
                    name="ProductPicture"
                    id="ProductPicture"
                    type="file"
                    multiple
                    onChange={this._handleUploadImageProduct}
                    value={files}
                  ></input>
                </Col>
              </Row>
              {imageShowClientState ? (
                <Row className="layout__Modal-addProduct_Upload-row">
                  <Col span={4}></Col>
                  <Col
                    span={20}
                    className="layout__Modal-addProduct_Upload-Image"
                  >
                    <Row>
                      {imageShowClientState.map((item: any, index: number) => (
                        <ShowImageClient
                          className={`layout__Modal-addProduct_Upload-Image-box`}
                          keyImage={index}
                          key={index}
                          item={item}
                          handleDeleteImage={this.handleDeleteImage}
                        />
                      ))}
                    </Row>
                  </Col>
                </Row>
              ) : (
                <div></div>
              )}
            </Col>
          </Row>
          <Row className="layout__Modal-addProduct_SaveReset">
            <Col span={20}></Col>
            <Col span={4}>
              <Row>
                <Col
                  span={12}
                  className="layout__Modal-addProduct_SaveReset--button"
                >
                  <button>Reset</button>
                </Col>
                <Col
                  span={12}
                  className="layout__Modal-addProduct_SaveReset--button"
                >
                  <button>Lưu</button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Modal>
      </>
    );
  }
}

export default connect(
  (state: any) => ({
    brandsAllReducers: state.brandsAllReducers,
    categoriesAllReducers: state.categoriesAllReducers,
    categoriesChildrenReducers: state.categoriesChildrenReducers,
  }),
  (dispatch: Dispatch) => ({
    actionsCategories: bindActionCreators(categoriesActions, dispatch),
    actionsBrands: bindActionCreators(brandsActions, dispatch),
  })
)(AddProduct);
