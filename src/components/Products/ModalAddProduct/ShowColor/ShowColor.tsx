import React, { Component } from "react";

// import ant design
import { Col } from "antd";
//Import Icon
import HighlightOffIcon from "@material-ui/icons/HighlightOff";

interface IProps {
  item: string;
  keyCodeColor: number;
  handleDeleteColor: (keyCodeColor: number) => void;
}
class ShowColor extends Component<IProps> {
  handleDeleteColor = () => {
    let { keyCodeColor } = this.props;
    this.props.handleDeleteColor(keyCodeColor);
  };
  render() {
    let { item } = this.props;
    return (
      <>
        <div className="layout__Modal-addProduct_boxItemColor">
          <Col span={24} className="layout__Modal-addProduct_boxItemColor-col">
            <div
              className="layout__Modal-addProduct_boxItemColor-col-detail"
              style={{ background: `#${item}` }}
            ></div>
            <span>{item}</span>
            <HighlightOffIcon
              style={{
                position: "absolute",
                top: "0.1em",
                right: "0.1em",
                color: "red",
                fontSize: "20px",
              }}
              onClick={this.handleDeleteColor}
            />
          </Col>
        </div>
      </>
    );
  }
}

export default ShowColor;
