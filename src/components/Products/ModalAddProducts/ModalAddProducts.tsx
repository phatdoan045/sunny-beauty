import React, { Component } from "react";
import { Modal, Row, Col, Select, Input } from "antd";

// import Redux
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";
import { brandsActions } from "../../../stores/actions/Brands";
import { categoriesActions } from "../../../stores/actions/Categories";

// import Icon
import CategoryIcon from "@material-ui/icons/Category";
import EmojiFoodBeverageIcon from "@material-ui/icons/EmojiFoodBeverage";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";

import "./ModalAddProducts.scss";
import InputCustom from "../../../componentsUtilities/InputCustom/InputCustom";
import InputNumber from "../../../componentsUtilities/InputNumber/InputNumber";

interface product {
  name: string;
  englishname: string;
  categoryId: string;
  description: string;
  origin?: string;
  brandId: string;
  inStock: number;
  capacity?: string;
  color: any;
  importPriceUSD: number;
  importPriceVND: number;
  importFee: number;
  retailPrice: number;
  wholeSalePrice: number;
  barcode: number;
  status: number;
  productImage?: [];
}
interface brandsAllReducers {
  brands: [];
}
interface brands {
  description?: string;
  brandImage?: string;
  _id: string;
  name: string;
}
interface category {
  description?: string;
  parentId?: string;
  _id: string;
  name: string;
}

interface categoriesAllReducers {
  categories: [];
}

interface IProps {
  visible: boolean;
  handleAddProduct: () => void;

  categoriesAllReducers: categoriesAllReducers;
  brandsAllReducers: brandsAllReducers;
  categoriesChildrenReducers: categoriesAllReducers;
  actionsCategories: any;
  actionsBrands: any;
}
interface IState {
  imageShowClientState: any;
  name: string;
  englishname: string;
  categoryId: string;
  description: string;
  origin: string;
  brandId: string;
  inStock: string;
  capacity?: string;
  color: any;
  importPriceUSD: string;
  importPriceVND: string;
  importFee: string;
  retailPrice: string;
  wholeSalePrice: string;
  barcode: string;
  status: string;
  productImage?: [];
  cost: string;
  valueColor: string;
  files: any;
  categoryIdChilren: string;
  categoryIdParent: string;
  colorUiCategory: boolean;
}

const { Option } = Select;
const selectUnit = (
  <Select defaultValue="kg" className="select-after">
    <Option value="0">kg</Option>
    <Option value="1">g</Option>
    <Option value="2">ml</Option>
  </Select>
);

class ModalAddProducts extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      imageShowClientState: [],
      name: "",
      englishname: "",
      categoryId: "",
      description: "",
      origin: "",
      brandId: "",
      inStock: "",
      capacity: "",
      cost: "",
      color: [],
      importPriceUSD: "",
      importPriceVND: "",
      importFee: "",
      retailPrice: "",
      wholeSalePrice: "",
      barcode: "",
      status: "",
      valueColor: "",
      productImage: [],
      files: [],
      categoryIdChilren: "",
      categoryIdParent: "",
      colorUiCategory: false,
    };
  }

  componentDidMount() {
    this.props.actionsBrands.getListBrandAllAPI();
    this.props.actionsCategories.getListCategoriesAllAPI();
  }

  handleCloseModal = () => {
    this.props.handleAddProduct();
  };

  handleChangeCategory = (value: string) => {
    this.props.actionsCategories.getCategoriesByIdChildren(value);
    if (!this.props.categoriesChildrenReducers.categories) {
      this.setState((prevState) => ({
        ...prevState,
        categoryIdParent: value,
        categoryId: "",
      }));
    }
    this.setState((prevState) => ({
      ...prevState,
      categoryId: "",
    }));
  };

  handleChangeBrands = (value: string) => {
    this.setState((prevState) => ({
      ...prevState,
      brandId: value,
    }));
  };

  handleChangeCategoryChildren = (value: string) => {
    this.setState((prevState) => ({
      ...prevState,
      categoryIdChilren: value,
      categoryId: value,
    }));
  };

  handleChangeInput = (event: any) => {
    event.persist();
    this.setState((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  handleChangeInputNumber = (value: any, nameInput: string) => {
    this.setState((prevState) => ({
      ...prevState,
      [nameInput]: value.value,
    }));
  };

  render() {
    let {
      brandId,
      categoryId,
      categoryIdChilren,
      categoryIdParent,
      name,
      cost,
      importPriceVND,
      importPriceUSD,
      importFee,
      retailPrice,
      wholeSalePrice,
      origin,
      barcode,
      inStock,
      capacity,
    } = this.state;
    let {
      visible,
      categoriesAllReducers,
      categoriesChildrenReducers,
      brandsAllReducers,
    } = this.props;
    console.log(categoryId);

    let color = false;
    if (brandId && categoryId) {
      color = true;
    }
    return (
      <>
        <Modal
          destroyOnClose={true}
          visible={visible}
          cancelButtonProps={{ style: { display: "none" } }}
          okButtonProps={{ style: { display: "none" } }}
          footer={null}
          maskClosable={false}
          onCancel={this.handleCloseModal}
          width={"65%"}
          style={{ top: "65px" }}
        >
          <Row className="layout_modal">
            <Col span={3} className="layout_modal_title">
              <Col
                span={24}
                className={`layout_modal_title-categogry layout_modal_title--boder ${
                  color ? "layout_modal_title_complete" : ""
                }`}
              >
                <Row className="layout_modal_title_box">
                  <Col className={`layout_modal_title_box_icons `} span={24}>
                    <CategoryIcon
                      style={{
                        margin: "0 auto",
                        left: "0px",
                        right: "0px",
                        display: "block",
                        fontSize: "24px",
                      }}
                    />
                  </Col>
                  <Col className="layout_modal_title_box_name" span={24}>
                    <p>Thể loại</p>
                  </Col>
                </Row>
              </Col>
              <Col
                span={24}
                className="layout_modal_title-inforProduct layout_modal_title--boder"
              >
                <Row className="layout_modal_title_box">
                  <Col className="layout_modal_title_box_icons" span={24}>
                    <EmojiFoodBeverageIcon
                      style={{
                        margin: "0 auto",
                        left: "0px",
                        right: "0px",
                        display: "block",
                        fontSize: "24px",
                      }}
                    />
                  </Col>
                  <Col className="layout_modal_title_box_name" span={24}>
                    <p>Sản phẩm</p>
                  </Col>
                </Row>
              </Col>
              <Col
                span={24}
                className="layout_modal_title-image layout_modal_title--boder"
              >
                <Row className="layout_modal_title_box">
                  <Col className="layout_modal_title_box_icons" span={24}>
                    <CloudUploadIcon
                      style={{
                        margin: "0 auto",
                        left: "0px",
                        right: "0px",
                        display: "block",
                        fontSize: "24px",
                      }}
                    />
                  </Col>
                  <Col className="layout_modal_title_box_name" span={24}>
                    <p>Hình ảnh</p>
                  </Col>
                </Row>
              </Col>
            </Col>

            <Col span={21} className="layout_modal_detail">
              <Row className="layout_modal_detail_title">
                <Col span={24} className="layout_modal_detail_title_icons">
                  <CategoryIcon />
                  <span>Danh mục</span>
                </Col>
                <Col span={24} className="layout_modal_detail_content">
                  <Row>
                    <Col span={12}>
                      <Col
                        span={24}
                        className="layout_modal_detail_content--title"
                      >
                        Danh mục <span>*</span>
                      </Col>
                      <Col
                        span={24}
                        className="layout_modal_detail_content--select"
                      >
                        <Select
                          showSearch
                          style={{ width: "90%" }}
                          placeholder="Danh mục"
                          optionFilterProp="children"
                          onChange={this.handleChangeCategory}
                          value={
                            categoryIdParent ? categoryIdParent : undefined
                          }
                        >
                          {categoriesAllReducers
                            ? categoriesAllReducers.categories.map(
                                (item: category, index: number) =>
                                  !item.parentId ? (
                                    <Option key={index} value={item._id}>
                                      {item.name}
                                    </Option>
                                  ) : null
                              )
                            : null}
                        </Select>
                      </Col>
                    </Col>
                    <Col span={12}>
                      <Col
                        span={24}
                        className="layout_modal_detail_content--title"
                      >
                        Thương hiệu <span>*</span>
                      </Col>
                      <Col
                        span={24}
                        className="layout_modal_detail_content--select"
                      >
                        <Select
                          showSearch
                          style={{ width: "90%" }}
                          placeholder="Danh mục"
                          optionFilterProp="children"
                          onChange={this.handleChangeBrands}
                          value={brandId ? brandId : undefined}
                        >
                          {brandsAllReducers
                            ? brandsAllReducers.brands.map(
                                (item: brands, index: number) => (
                                  <Option key={index} value={item._id}>
                                    {item.name}
                                  </Option>
                                )
                              )
                            : null}
                        </Select>
                      </Col>
                    </Col>
                    <Col span={24}>
                      {categoriesChildrenReducers.categories.length >= 1 ? (
                        <>
                          <Col
                            span={24}
                            className="layout_modal_detail_content--title"
                          >
                            Danh mục con <span>*</span>
                          </Col>
                          <Col
                            span={24}
                            className="layout_modal_detail_content--select"
                          >
                            <Select
                              showSearch
                              style={{ width: "45%" }}
                              placeholder="Danh mục con"
                              optionFilterProp="children"
                              onChange={this.handleChangeCategoryChildren}
                              value={
                                categoryIdChilren
                                  ? categoryIdChilren
                                  : undefined
                              }
                            >
                              {categoriesChildrenReducers.categories.map(
                                (item: category, index: number) =>
                                  item.parentId ? (
                                    <Option key={index} value={item._id}>
                                      {item.name}
                                    </Option>
                                  ) : null
                              )}
                            </Select>
                          </Col>
                        </>
                      ) : null}
                    </Col>
                  </Row>
                </Col>
              </Row>

              {/* Product Infor */}
              <Row className="layout_modal_detail_title layout_modal_detail--magrin">
                <Col span={24} className="layout_modal_detail_title_icons">
                  <EmojiFoodBeverageIcon />
                  <span>Thông tin sản phẩm</span>
                </Col>
                <Col span={24} className="layout_modal_detail_content">
                  <Row className="layout_modal_detail_content--Row">
                    <Col span={12}>
                      <InputCustom
                        placeholder="Nhập tên sản phẩm"
                        handleChangeInput={this.handleChangeInput}
                        valueInput={name}
                        nameTitle="Tên sản phẩm"
                        nameInput="name"
                        classNameBoxInput="layout_modal_detail_content--input"
                        classNameTitle="layout_modal_detail_content--title"
                      />
                    </Col>
                    <Col span={12}>
                      <InputCustom
                        placeholder="Nhập tên sản phẩm English"
                        handleChangeInput={this.handleChangeInput}
                        valueInput={name}
                        nameTitle="Tên sản phẩm English"
                        nameInput="name"
                        classNameBoxInput="layout_modal_detail_content--input layout_modal_detail_content--input-right"
                        classNameTitle="layout_modal_detail_content--title "
                      />
                    </Col>
                  </Row>
                  <Row className="layout_modal_detail_content--Row">
                    <Col span={8} className="layout_modal_detail_content--Col">
                      <InputNumber
                        handleChangeInput={this.handleChangeInputNumber}
                        valueInput={cost}
                        nameTitle="Giá sản phẩm"
                        nameInput="cost"
                        placeholder="Nhập giá sản phẩm"
                        classNameBoxInput="layout_modal_detail_content--input layout_modal_detail_content--input-right"
                        classNameTitle="layout_modal_detail_content--title "
                      />
                    </Col>
                    <Col span={8} className="layout_modal_detail_content--Col">
                      <InputNumber
                        handleChangeInput={this.handleChangeInputNumber}
                        valueInput={importPriceVND}
                        nameTitle="Giá nhập hàng VND"
                        nameInput="importPriceVND"
                        placeholder="Nhập giá nhập hàng VND"
                        classNameBoxInput="layout_modal_detail_content--input layout_modal_detail_content--input-right"
                        classNameTitle="layout_modal_detail_content--title "
                      />
                    </Col>

                    <Col span={8} className="layout_modal_detail_content--Col">
                      <InputNumber
                        handleChangeInput={this.handleChangeInputNumber}
                        valueInput={importPriceUSD}
                        nameTitle="Giá nhập hàng USD"
                        nameInput="importPriceUSD"
                        placeholder="Nhập giá nhập hàng USD"
                        classNameBoxInput="layout_modal_detail_content--input layout_modal_detail_content--input-right"
                        classNameTitle="layout_modal_detail_content--title "
                      />
                    </Col>
                  </Row>
                  <Row className="layout_modal_detail_content--Row">
                    <Col span={8} className="layout_modal_detail_content--Col">
                      <InputNumber
                        handleChangeInput={this.handleChangeInputNumber}
                        valueInput={retailPrice}
                        nameTitle="Giá bán lẻ"
                        nameInput="retailPrice"
                        placeholder="Nhập giá bán lẻ"
                        classNameBoxInput="layout_modal_detail_content--input layout_modal_detail_content--input-right"
                        classNameTitle="layout_modal_detail_content--title "
                      />
                    </Col>
                    <Col span={8} className="layout_modal_detail_content--Col">
                      <InputNumber
                        handleChangeInput={this.handleChangeInputNumber}
                        valueInput={wholeSalePrice}
                        nameTitle="Giá bán sỉ"
                        nameInput="wholeSalePrice"
                        placeholder="Nhập giá bán sỉ"
                        classNameBoxInput="layout_modal_detail_content--input layout_modal_detail_content--input-right"
                        classNameTitle="layout_modal_detail_content--title "
                      />
                    </Col>

                    <Col span={8} className="layout_modal_detail_content--Col">
                      <InputNumber
                        handleChangeInput={this.handleChangeInputNumber}
                        valueInput={importFee}
                        nameTitle="Phí vận chuyển USD"
                        nameInput="importFee"
                        placeholder="Nhập phí vận chuyển USD"
                        classNameBoxInput="layout_modal_detail_content--input layout_modal_detail_content--input-right"
                        classNameTitle="layout_modal_detail_content--title "
                      />
                    </Col>
                  </Row>

                  <Row className="layout_modal_detail_content--Row">
                    <Col span={12}>
                      <InputCustom
                        placeholder="Nhập nguồn gốc"
                        handleChangeInput={this.handleChangeInput}
                        valueInput={origin}
                        nameTitle="Nguồn gốc sản phẩm"
                        nameInput="origin"
                        classNameBoxInput="layout_modal_detail_content--input"
                        classNameTitle="layout_modal_detail_content--title"
                      />
                    </Col>

                    <Col span={12}>
                      <InputCustom
                        placeholder="Nhập mã vạch sản phẩm"
                        handleChangeInput={this.handleChangeInput}
                        valueInput={barcode}
                        nameTitle="Mã vạch sản phẩm"
                        nameInput="barcode"
                        classNameBoxInput="layout_modal_detail_content--input layout_modal_detail_content--input-right"
                        classNameTitle="layout_modal_detail_content--title"
                      />
                    </Col>
                  </Row>

                  <Row className="layout_modal_detail_content--Row">
                    <Col span={12}>
                      <InputCustom
                        placeholder="Nhập tồn kho"
                        handleChangeInput={this.handleChangeInput}
                        valueInput={inStock}
                        nameTitle="Sản phẩm tồn kho"
                        nameInput="inStock"
                        classNameBoxInput="layout_modal_detail_content--input"
                        classNameTitle="layout_modal_detail_content--title"
                      />
                    </Col>

                    <Col span={12}>
                      <Col
                        span={24}
                        className="layout_modal_detail_content--title"
                      >
                        Dung lượng/ khối lượng <span>*</span>
                      </Col>
                      <Col
                        span={24}
                        className="layout_modal_detail_content--input"
                      >
                        <Input
                          addonAfter={selectUnit}
                          type="number"
                          placeholder="Nhập khối lượng / Dung tích"
                          value={capacity}
                          onChange={this.handleChangeInput}
                          name="capacity"
                        />
                      </Col>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
        </Modal>
      </>
    );
  }
}
export default connect(
  (state: any) => ({
    brandsAllReducers: state.brandsAllReducers,
    categoriesAllReducers: state.categoriesAllReducers,
    categoriesChildrenReducers: state.categoriesChildrenReducers,
  }),
  (dispatch: Dispatch) => ({
    actionsCategories: bindActionCreators(categoriesActions, dispatch),
    actionsBrands: bindActionCreators(brandsActions, dispatch),
  })
)(ModalAddProducts);
