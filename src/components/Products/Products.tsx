import React, { Component } from "react";

// Import AnT Design
import { Row, Col, Pagination, Select } from "antd";
import { TagFilled } from "@ant-design/icons";

import "./Products.scss";

// Import Redux
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";
import { productsActions } from "../../stores/actions/Products";

import ItemProduct from "./ItemProduct/ItemProduct";

import AddIcon from "@material-ui/icons/Add";
// import AddProduct from "./ModalAddProduct/AddProduct";
import ModalAddProducts from "./ModalAddProducts/ModalAddProducts";

const { Option } = Select;
interface productReducers {
  products: [];
  totalPage: number;
}

interface brandsAllReducers {
  brands: [];
}
interface IProps {
  productReducers: productReducers;

  actionsProducts: any;
}
interface IState {
  page: number;
  limit: number;
  showModalAddProduct: boolean;
}
class Products extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      page: 1,
      limit: 10,
      showModalAddProduct: false,
    };
  }
  componentDidMount() {
    let { page, limit } = this.state;
    this.props.actionsProducts.getListProductAPI(page, limit);
  }
  handleChangePagination = (page: number) => {
    this.setState({
      page,
    });
    this.props.actionsProducts.getListProductAPI(page, this.state.limit);
  };
  handleAddProduct = () => {
    this.setState({
      showModalAddProduct: !this.state.showModalAddProduct,
    });
  };
  handleLimitPagination = (value: string) => {
    let valueParseInt = parseInt(value);
    this.setState({
      limit: valueParseInt,
    });
    this.props.actionsProducts.getListProductAPI(1, valueParseInt);
  };
  render() {
    let listProducts = this.props.productReducers.products;
    let totalPage = this.props.productReducers.totalPage;

    let { page, limit, showModalAddProduct } = this.state;

    return (
      <>
        <div className="layout__Products">
          <Row className="layout__Products-title">
            <Col className="layout__Products-title-left" span={14}>
              <span>
                <TagFilled />
              </span>
              Sản Phẩm
            </Col>
            <Col className="layout__Products-title-right" span={10}>
              <Row className="layout__Products-title-rightRow">
                <Col
                  span={12}
                  className="layout__Products-title-right-addProduct"
                >
                  <button onClick={this.handleAddProduct}>
                    <AddIcon />
                    <span>Thêm sản phẩm</span>
                  </button>
                </Col>
                <Col
                  span={12}
                  className="layout__Products-title-right-exportImport"
                ></Col>
              </Row>
            </Col>
          </Row>
          <Row className="layout__Products-table">
            <table style={{ width: "100%" }}>
              <thead>
                <tr className="layout__Products-table-rowTitle">
                  <th>Số TT</th>
                  <th>Tên sản phẩm</th>
                  <th>Ảnh</th>
                  <th>Tồn kho</th>
                  <th>Mã vạch</th>
                  <th>Trạng thái</th>
                  <th>Thao tác</th>
                </tr>
              </thead>
              <tbody>
                {listProducts ? (
                  listProducts.map((item, index) => (
                    <ItemProduct
                      product={item}
                      keyProduct={index}
                      key={index}
                      page={page}
                      limit={limit}
                    />
                  ))
                ) : (
                  <p>No Product</p>
                )}
              </tbody>
            </table>
          </Row>
          <Row className="layout__Products-Pagination">
            <Col span={24} className="layout__Products-Pagination-col">
              <Pagination
                current={page}
                pageSize={limit}
                total={totalPage}
                onChange={(page) => this.handleChangePagination(page)}
              />

              <Select
                defaultValue="10"
                style={{ width: 65, float: "right", marginRight: "10px" }}
                onChange={this.handleLimitPagination}
              >
                <Option value="1">1</Option>
                <Option value="10">10</Option>
                <Option value="20">20</Option>
                <Option value="30">30</Option>
              </Select>
            </Col>
          </Row>
        </div>
        {/* <AddProduct
          visible={showModalAddProduct}
          handleAddProduct={this.handleAddProduct}
        /> */}

        <ModalAddProducts
          visible={showModalAddProduct}
          handleAddProduct={this.handleAddProduct}
        />
      </>
    );
  }
}

export default connect(
  (state: any) => ({
    productReducers: state.productReducers,
  }),
  (dispatch: Dispatch) => ({
    actionsProducts: bindActionCreators(productsActions, dispatch),
  })
)(Products);
