import React, { Component } from "react";
import { Layout, Menu, Row, Col } from "antd";

import {
  ShoppingFilled,
  HomeFilled,
  GiftFilled,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from "@ant-design/icons";
import "./Skeleton.scss";

import logo from "../../assets/images/pages/logoYumi.jpg";
import fullLogo from "../../assets/images/pages/yumi_full.jpg";
import Products from "../Products/Products";

const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;

interface IProps {}
interface IState {
  collapsed: boolean;
}

class Skeleton extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      collapsed: true,
    };
  }
  handleToggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  render() {
    return (
      <Layout className="layout">
        <Sider
          collapsible
          collapsedWidth={64}
          collapsed={this.state.collapsed}
          className="layout__sider"
        >
          <div className="logo">
            <img src={this.state.collapsed ? logo : fullLogo} alt="logo"></img>
          </div>
          <Menu
            theme={"light"}
            mode="inline"
            defaultSelectedKeys={["1"]}
            className={
              this.state.collapsed
                ? "layout__menu"
                : "layout__menu-collapsed_faile"
            }
          >
            <Menu.Item key="1" icon={<HomeFilled />}>
              Home
            </Menu.Item>
            <Menu.Item key="2" icon={<ShoppingFilled />}>
              Shopping
            </Menu.Item>
            <SubMenu key="3" icon={<GiftFilled />} title="Gift Cart">
              <Menu.Item key="4">Create Gift Cart</Menu.Item>
              <Menu.Item key="5">List Gift Cart</Menu.Item>
            </SubMenu>
          
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            <Row>
              <Col span={12}>
                {React.createElement(
                  this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                  {
                    className: "trigger",
                    onClick: this.handleToggle,
                  }
                )}
              </Col>
              <Col span={12}></Col>
            </Row>
          </Header>
          <Content
            // className="site-layout-background"
            style={{
              margin: "24px 16px",
              padding: 24,
              background:"#ffffff"
            }}
          >
            <Products />
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default Skeleton;
