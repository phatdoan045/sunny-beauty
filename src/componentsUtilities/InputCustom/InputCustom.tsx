import React, { Component } from "react";
import { Col } from "antd";

interface IProps {
  classNameTitle: string;
  nameTitle: string;
  classNameBoxInput: string;
  handleChangeInput: (event: any) => void;
  nameInput: string;
  valueInput: string;
  placeholder:string;
}

class InputCustom extends Component<IProps> {
  render() {
    let {
      classNameTitle,
      classNameBoxInput,
      nameTitle,
      nameInput,
      valueInput,
      placeholder,
      handleChangeInput,
    } = this.props;
    return (
      <>
        <Col span={24} className={classNameTitle}>
          {nameTitle} <span>*</span>
        </Col>
        <Col span={24} className={classNameBoxInput}>
          <input
            placeholder={placeholder}
            name={nameInput}
            value={valueInput}
            onChange={handleChangeInput}
          />
        </Col>
      </>
    );
  }
}

export default InputCustom;
