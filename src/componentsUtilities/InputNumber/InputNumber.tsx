import React, { Component } from "react";
import NumberFormat from "react-number-format";

import { Col } from "antd";

interface IProps {
  classNameTitle: string;
  nameTitle: string;
  classNameBoxInput: string;
  nameInput: string;
  valueInput: any;
  placeholder:string;
  handleChangeInput:(value:any,nameInput:string)=>void;
}

class InputNumber extends Component<IProps> {
  render() {
    let {
      classNameTitle,
      classNameBoxInput,
      nameTitle,
      nameInput,
      valueInput,
      placeholder,
      handleChangeInput,
    } = this.props;
    return (
      <>
        <Col span={24} className={classNameTitle}>
          {nameTitle} <span>*</span>
        </Col>
        <Col span={24} className={classNameBoxInput}>
         
          <NumberFormat
            inputmode="numeric"
            name={nameInput}
            placeholder={placeholder}
            value={valueInput}
            thousandSeparator={true}
            onValueChange={(value) => handleChangeInput(value, nameInput)}
          />
        </Col>
      </>
    );
  }
}

export default InputNumber;
