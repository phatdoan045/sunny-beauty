import React, { Component } from "react";

import { Row, Col } from "antd";
interface IProps {
  classnameRow: string;
  numberSpanColTitle: number;
  classNameColTitle: string;
  nameTitle: string;

  numberSpanColContent: number;
  classNameColContent: string;
  typeInput?: string;
  clasnameInputContent?:string;
  name:string
  value:any;
 
  handleChangeInput:(event:any)=>void;
}
class InputUtil extends Component<IProps> {
  render() {
    let {
      classnameRow,
      numberSpanColTitle,
      classNameColTitle,
      nameTitle,
      numberSpanColContent,
      classNameColContent,
      typeInput,
      clasnameInputContent,
      name,
      value,
      handleChangeInput
    } = this.props;
    return (
      <div>
        <Row className={classnameRow}>
          <Col span={numberSpanColTitle} className={classNameColTitle}>
            {nameTitle}
            <span>(*)</span>
          </Col>
          <Col span={numberSpanColContent} className={classNameColContent}>
            <input value={value}  name={name} placeholder={nameTitle} type={typeInput} className={clasnameInputContent}
            onChange={handleChangeInput}></input>            
          </Col>
        </Row>
      </div>
    );
  }
}

export default InputUtil;
