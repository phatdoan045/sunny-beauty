import React, { Component } from "react";
import { Col} from "antd";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import './ShowImageClient.scss'
interface IProps{
    item:any;
    keyImage:number;
    className:string;
    handleDeleteImage:(keyImage:number)=>void;
}
class ShowImageClient extends Component<IProps> {
  handleDeleteImagebyKey=()=>{
     this.props.handleDeleteImage(this.props.keyImage);
  }
  render() {
    let{item, className, keyImage} = this.props;
    return (
      <>
        <Col span={4} className={className} style={{position:"relative"}} key={keyImage}>
          <img src={item} alt="productInfor" />
          <HighlightOffIcon className="layout__ShowProductImageClient" onClick={this.handleDeleteImagebyKey}/>
        </Col>
      </>
    );
  }
}

export default ShowImageClient;
