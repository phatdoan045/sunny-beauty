import {notification   } from 'antd';

export const deleteItemByIndex = (Array:any,index:number) =>{
    Array.splice(index,1);
    return Array;
}

export const formatNumberPrice =(number:string)=> {
    return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

export const openNotificationError = (message:string, description:string) => {
    notification.error({
      message: message,
      description: description
    });
};

export const openNotificationSuccess = (message:string, description:string) => {
    notification.error({
      message: message,
      description: description
    });
};