export const NETWORK_ERROR = "NETWORK_ERROR!";
export const NETWORK_ERROR_DESCRIPTION = "Không thể kết nối đến máy chủ, Vui lòng kiểm tra đường truyền mạng";

// Action Brands
    // -> Success

    // -> Faile
    export const GET_ALL_BRAND_FAILE = "GET_ALL_BRAND_FAILE";
    export const GET_ALL_BRAND_FAILE_DESCRIPTION = "Lỗi không thể lấy dữ liệu brands từ server!! Chúng tôi sẽ khác phục ngay";

// Action Categories
    // -> Success

    // -> Faile
    export const GET_ALL_CATEGORIES_FAILE = "GET_ALL_CATEGORIES_FAILE";
    export const GET_ALL_CATEGORIES_FAILE_DESCRIPTION = "Lỗi không thể lấy dữ liệu thể loại từ server!! Chúng tôi sẽ khác phục ngay";
