import * as types from "../constants/indexConstants";
import API from "../../configAPI/Api";

export const getListBrandAllAPI = () => {
  return (dispatch: any) => {
    return API.get({ endpoint: `brands/all` }, (status: any, res: any) => {
      if (status === 200) {
        dispatch(getListBrandSuccess(res.data));
      } else {
        dispatch(getListBrandFaile());
      }
    });
  };
};

export const getListBrandSuccess = (listBrands: any) => {
  return {
    type: types.GET_LISTALL_BRAND_SUCCSESS,
    listBrands,
  };
};

export const getListBrandFaile = () => {
  return {
    type: types.GET_LISTALL_BRAND_FAILE,
  };
};

export const brandsActions = {
  getListBrandAllAPI,
};
