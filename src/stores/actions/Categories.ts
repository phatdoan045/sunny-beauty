import * as types from "../constants/indexConstants";
import API from "../../configAPI/Api";

export const getListCategoriesAllAPI = () => {
  return (dispatch: any) => {
    return API.get({ endpoint: `categories/all` }, (status: any, res: any) => {
      if (status === 200) {
        dispatch(getListCategoriesSuccess(res.data));
      } else {
        dispatch(getListCategoriesFaile());
      }
    });
  };
};

export const getListCategoriesSuccess = (listCategories: any) => {
  return {
    type: types.GET_LISTALL_CATEGORIES_SUCCSESS,
    listCategories,
  };
};

export const getListCategoriesFaile = () => {
  return {
    type: types.GET_LISTALL_CATEGORIES_FAILE,
  };
};

export const getCategoriesByIdChildren = (parentId: string) => {
  return (dispatch: any) => {
    return API.get(
      { endpoint: `categories/${parentId}/children` },
      (status: any, res: any) => {
        if (status === 200) {
          dispatch(getCategoriesByIdChildrenSuccess(res.data));
        } else {
          dispatch(getCategoriesByIdChildrenFaile());
        }
      }
    );
  };
};

export const getCategoriesByIdChildrenSuccess = (listCategories: any) => {
  return {
    type: types.GET_CHILDREN_CATEGORIES_SUCCSESS,
    listCategories,
  };
};

export const getCategoriesByIdChildrenFaile = () => {
  return {
    type: types.GET_CHILDREN_CATEGORIES__FAILE,
  };
};

export const categoriesActions = {
  getListCategoriesAllAPI,
  getCategoriesByIdChildren,
};
