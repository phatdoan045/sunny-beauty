import * as types from "../constants/indexConstants";

export const loadingStart = () => {
  return {
    type: types.LOADING_START,
  };
};

export const loadingEnd = () => {
  return {
    type: types.LOADING_END,
  };
};

export const loadingActions = {
  loadingStart,
  loadingEnd,
};
