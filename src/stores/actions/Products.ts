import * as types from "../constants/indexConstants";
import { loadingStart, loadingEnd } from "../actions/Loading";
import API from "../../configAPI/Api";

export const getListProductAPI = (nbPage: number, nbLimit: number) => {
  return (dispatch: any) => {
    return API.get(
      { endpoint: `product?page=${nbPage}&limit=${nbLimit}` },
      (status: any, res: any) => {
        dispatch(loadingStart());
        if (status === 200) {
          dispatch(getListProductSuccess(res.data, res.totalPages));
        } else {
          dispatch(getListProductFaile());
        }
        setTimeout(()=>dispatch(loadingEnd()),1000);
      }
    );
  };
};

export const getListProductSuccess = (listProduct: any, totalPage: number) => {
  return {
    type: types.GET_LISTALL_PRODUCT_SUCCSESS,
    listProduct,
    totalPage,
  };
};

export const getListProductFaile = () => {
  return {
    type: types.GET_LISTALL_PRODUCT_FAILE,
  };
};

export const productsActions = {
  getListProductAPI,
};
