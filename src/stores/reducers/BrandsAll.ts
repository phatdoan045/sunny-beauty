import * as types from "../constants/indexConstants";
import { openNotificationError } from "../../functionsUtilities/index";
import * as notification from "../../notification/notification";
const initialState: any = {
  brands: []
};

const brandsReducers = (state = initialState, action: any) => {
  switch (action.type) {
    case types.GET_LISTALL_BRAND_SUCCSESS:
      state.brands = action.listBrands;
      return { ...state };
    case types.GET_LISTALL_BRAND_FAILE:
      openNotificationError(
        notification.GET_ALL_BRAND_FAILE,
        notification.GET_ALL_BRAND_FAILE_DESCRIPTION
      );
      return { ...state };

    default:
      return state;
  }
};
export default brandsReducers;
