import * as types from "../constants/indexConstants";
import { openNotificationError } from "../../functionsUtilities/index";
import * as notification from "../../notification/notification";
const initialState: any = {
  categories: []
};

const categoriesReducers = (state = initialState, action: any) => {
  switch (action.type) {
    case types.GET_LISTALL_CATEGORIES_SUCCSESS:
      state.categories = action.listCategories;
      return { ...state };
    case types.GET_LISTALL_CATEGORIES_FAILE:
      openNotificationError(
        notification.GET_ALL_CATEGORIES_FAILE,
        notification.GET_ALL_CATEGORIES_FAILE_DESCRIPTION
      );
      return { ...state };

    default:
      return state;
  }
};
export default categoriesReducers;