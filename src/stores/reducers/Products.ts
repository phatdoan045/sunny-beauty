import * as types from "../constants/indexConstants";

const initialState: any = {
  products: [],
  totalPage: 0
};


const productReducers = (state = initialState, action: any) => {
    switch (action.type) {
    
      case types.GET_LISTALL_PRODUCT_SUCCSESS:
        state.products = action.listProduct[0].products;
        state.totalPage = action.totalPage;
        return { ...state };
      default:
        return state;
    }
  };
  export default productReducers;