import { combineReducers } from "redux";
import productReducers from '../reducers/Products'
import brandsAllReducers from '../reducers/BrandsAll'
import loadingReducers from '../reducers/Loading'
import categoriesAllReducers from '../reducers/CategoriesAll'
import categoriesChildrenReducers from '../reducers/CategoriesChildren'

const rootReducer = combineReducers({
   productReducers,
   brandsAllReducers,
   loadingReducers,
   categoriesAllReducers,
   categoriesChildrenReducers
});

export default rootReducer